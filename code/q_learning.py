"""
    Implements basic Q-learning with Q-table. Only works on discrete action
    and observation spaces.
"""
import gym
import random
import os
import numpy as np
import matplotlib.pyplot as plt
from time import sleep

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

def main(args):          
    env = gym.make(args['env'])
    obs_space = env.observation_space
    act_space = env.action_space
    
    assert type(act_space) is gym.spaces.Discrete and type(obs_space) is gym.spaces.Discrete, \
        "Q-learning only implemented for discrete action- and observation spaces"

    clear()  
    print("Performing Q-learning on OpenAI environment {}".format(args['env']))
    q_table = np.ones((obs_space.n, act_space.n))

    episodes = 10000
    log_interval = 100   # How often to print statistics
    done = False

    # Hyperparameters
    lr = 0.1
    gamma = 0.9
    #epsilon = 0.1 not used

    penalties = 0
    log_penalties = []
    log_rewards = []            # Rewards for one episode
    log_interval_rewards = []   # Rewards for all episodes up until log_interval
    total_rewards = []          # Averages of all log_interval rewards

    # Loop through episodes
    for episode in range(1, episodes + 1):
        obs = env.reset()
        done = False

        # Loop until current episode is finished
        while not done:
            # Choose an action
            if random.random() < 1 - episode/episodes:
                action = act_space.sample()
            else:
                action = np.argmax(q_table[obs])

            # Perform the action in the environment
            new_obs, reward, done, _ = env.step(action)
    
            # Update Q-table
            q_table[obs, action] = (1 - lr) * q_table[obs, action] + lr * (reward + gamma * np.max(q_table[new_obs]))
            
            # Reset observation variable and log reward
            obs = new_obs
            log_rewards.append(reward)
            if reward == -10:
                penalties += 1
        
        log_interval_rewards.append(np.sum(log_rewards))
        log_rewards = []

        if episode % log_interval == 0:
            print("Episode {} \t Penalties {}    \t Average reward {:.3f}".format(
                episode, penalties, np.mean(log_interval_rewards)
            ))
            total_rewards.append(np.mean(log_interval_rewards))
            log_penalties.append(penalties)
            log_interval_rewards = []
            penalties = 0

    env.close()

    # Plot average rewards
    x_range = range(log_interval, (len(total_rewards) + 1) * log_interval, log_interval)
    plt.plot(x_range, total_rewards)
    plt.show()

    """
    try:
        env.render()
    except NotImplementedError:
        print("Environment cannot be rendered")
        exit()
    """

    # Test the Q-table (no random action)
    for episode in range(1, 10):
        obs = env.reset()
        done = False
        timestep = 0

        # Loop until current episode is finished
        while not done:
            timestep += 1

            clear()
            try:
                env.render()
            except NotImplementedError:
                pass

            # Choose an action
            action = np.argmax(q_table[obs])

            # Perform the action in the environment
            new_obs, reward, done, _ = env.step(action)
            
            print("Timestep: {}\nState: {}\nAction: {}\nReward: {}".format(
                timestep, obs, action, reward
            ))

            obs = new_obs
            sleep(.2)

            if done:
                break

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('Perform Q-learning in an environment')
    parser.add_argument('--env', type=str, default='Taxi-v2',
                        help='An OpenAI environment to learn in. Default: Taxi-v2')
    args = parser.parse_args()
    main(vars(args))
