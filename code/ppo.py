"""
    Implements proximal policy optimization algorithm together with generalized
    advantage estimator.
"""
import gym
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from torch.nn.functional import relu, softmax

# The policy network
class PolicyFunction(nn.Module):
    def __init__(self, act_space, obs_space, hidden_size=32):
        super().__init__()

        self.fc1 = nn.Linear(obs_space, hidden_size)
        self.fc_pi = nn.Linear(hidden_size, act_space)
        self.fc_v = nn.Linear(hidden_size, 1)

    def forward(self, x):
        raise NotImplementedError

    def pi(self, x):
        """Gets an action using the policy from the given state
        """
        x = relu(self.fc1(x))
        x = softmax(self.fc_pi(x), dim=1)
        return x

    def v(self, x):
        """Gets the estimate of the value function for a given state
        """
        x = relu(self.fc1(x))
        x = self.fc_v(x)
        return x


class Trainer():    
    def __init__(self, obs_space, act_space, lr=0.001, gamma=0.98, lmbda=0.95, epsilon=0.1):
        # Set hyperparameters
        self.learning_rate = lr
        self.gamma = gamma
        self.lmbda = lmbda
        self.epsilon= epsilon

        # Set the policy- and value function
        self.policy = PolicyFunction(act_space, obs_space)
        self.old_policy = PolicyFunction(act_space, obs_space)

        # Each with their own optimizer
        self.policy_optim = torch.optim.Adam(self.policy.parameters(), lr=lr)

        # Initialize memory
        self.data = []

    def store_transition(self, transition):
        self.data.append(transition)

    def get_batch(self):
        obs_lst, act_lst, rew_lst, newobs_lst, prob_lst, done_lst = [], [], [], [], [], []

        for transition in self.data:
            obs, act, rew, new_obs, prob, done = transition

            obs_lst.append(obs)
            act_lst.append([act])
            rew_lst.append([rew])
            newobs_lst.append(new_obs)
            prob_lst.append([prob])
            done_lst.append([0] if done is True else [1])   # Invert because multiply
        self.data = []

        return torch.tensor(obs_lst, dtype=torch.float), torch.tensor(act_lst), \
            torch.tensor(rew_lst, dtype=torch.float), torch.tensor(newobs_lst, dtype=torch.float), \
            torch.tensor(prob_lst), torch.tensor(done_lst, dtype=torch.float)

    def choose_action(self, observation):
        """Select an action using the policy network. Returns
        the action and the probabilities.
        """
        observation = torch.tensor(observation, dtype=torch.float)
        probabilities = self.policy.pi(observation.reshape(1, -1)).squeeze()
        action_prob = torch.distributions.Categorical(probabilities)
        action = action_prob.sample().item()
        return action, probabilities

    def learn(self, k=3):
        """Runs update K amount of times"""
        obs, actions, rewards, new_obs, probabilities, done = self.get_batch()

        for i in range(k):
            # Compute the delta values
            td_target = rewards + self.gamma * self.policy.v(new_obs) * done
            delta = td_target - self.policy.v(obs)
            delta = delta.detach().numpy()

            # Compute the advantage function
            advantages = []
            advantage = 0.
            for delta_t in delta[::-1]:
                advantage = self.gamma * self.lmbda * advantage + delta_t[0]
                advantages.append([advantage])
            advantages.reverse()
            advantages = torch.tensor(advantages, dtype=torch.float)

            # Compute the ratio
            new_pi = self.policy.pi(obs)
            new_pi = new_pi.gather(1, actions)
            ratio = torch.exp(torch.log(new_pi) - torch.log(probabilities))

            # Compute surrogate function
            surr1 = ratio * advantages
            surr2 = torch.clamp(ratio, 1 - self.epsilon, 1 + self.epsilon) * advantages

            # Value function is minimized with a mean squared error
            grad = -torch.min(surr1, surr2) + torch.nn.functional.smooth_l1_loss(self.policy.v(obs), td_target.detach())
            grad = grad.mean()
            
            # Backpropagate policy gradient
            self.policy_optim.zero_grad()
            grad.backward()
            self.policy_optim.step()


def main(args):      
    episodes = 10000
    timesteps = 20
    log_interval = 50 # How often to print statistics

    episode_rewards = []
    log_interval_rewards = []
    total_rewards = []
    render_episode = True

    env = gym.make(args['env'])
    obs_space = env.observation_space.shape[0]
    act_space = env.action_space.n
    print("Observation space: {}\nAction space: {}".format(obs_space, act_space))

    agent = Trainer(obs_space, act_space)

    # Loop through episodes
    for episode in range(1, episodes + 1):
        obs = env.reset()
        done = False
        while not done:
            # Loop until current episode is finished
            for t in range(1, timesteps + 1):
                        
                # Render every log_interval episode
                if render_episode:
                    env.render()

                # Perform the action in the environment
                action, prob = agent.choose_action(obs)
                new_obs, reward, done, _ = env.step(action)
        
                # Store the transition
                agent.store_transition((obs, action, reward/100, new_obs, prob[action], done))
                episode_rewards.append(reward)
                obs = new_obs

                if done:
                    break
            
            agent.learn()  

        # Every episode we update our model and save statistics
        log_interval_rewards.append(sum(episode_rewards))  
        episode_rewards = []     
        render_episode = False          

        if episode % log_interval == 0:
            print("Episode {} \t Rendered reward {:.3f}  \t Average reward {:.3f}".format(
                episode, log_interval_rewards[0], np.mean(log_interval_rewards)
            ))
            total_rewards.append(np.mean(log_interval_rewards))
            log_interval_rewards = []
            render_episode = True

    env.close()
    x_range = range(log_interval, (len(total_rewards) + 1) * log_interval, log_interval)
    plt.plot(x_range, total_rewards)
    plt.show()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('Perform deep Q-learning in an environment')
    parser.add_argument('--env', type=str, default='CartPole-v1',
                        help='An OpenAI environment to learn in. Default: CartPole-v0')
    args = parser.parse_args()
    main(vars(args))