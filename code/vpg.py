"""
    Implements basic vanilla policy gradient algorithm
"""
import gym
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch import nn

# The policy network
class PolicyFunction(nn.Module):
    def __init__(self, act_space, obs_space, hidden_size=32):
        super().__init__()

        self.network = nn.Sequential(
            nn.Linear(obs_space, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, act_space),
            nn.Softmax(dim=0)
        )

    def forward(self, x):
        x = self.network(x)
        return x


class Trainer():
    def __init__(self, obs_space, act_space, lr=1e-2, gamma=0.99):
        self.learning_rate = lr
        self.gamma = gamma

        # Set the policy
        self.policy = PolicyFunction(act_space, obs_space)
        self.policy_optim = torch.optim.Adam(self.policy.parameters(), lr=lr)

        self.reward_memory = []
        self.action_memory = []
        self.state_memory = []

    def store_rewards(self, reward):
        self.reward_memory.append(reward)

    def store_state(self, state):
        self.state_memory.append(state)

    def reset_memory(self):
        self.action_memory = []
        self.reward_memory = []
        self.state_memory = []

    def discounted_rewards(self, rew):
        #Calculate discounted rewards for a sequence.
        discounted_rew = []
        for n in range(len(rew)):
            discounted_rew.append(self.gamma**n * rew[n])
        return discounted_rew

    def choose_action(self, observation):
        # Select an action using the policy network
        observation = torch.tensor(observation, dtype=torch.float)
        probabilities = self.policy(observation)
        action_prob = torch.distributions.Categorical(probabilities)
        action = action_prob.sample()
        self.action_memory.append(action_prob.log_prob(action))
        return action.item()

    def learn(self):
        # Calculates the gradient and updates parameters
        self.policy_optim.zero_grad()

        # Calculate the discounted reward for multiple trajectories
        R = np.zeros_like(self.reward_memory, dtype=np.float64)
        for t in range(len(self.reward_memory)):
            R_sum = 0

            for k in range(t, len(self.reward_memory)):
                R_sum += self.gamma**(k-t) * self.reward_memory[k]
            R[t] = R_sum

        # Normalize the rewards and transform it to a tensor
        R -= R.mean()
        R /= R.std() + 1e-7
        R = torch.tensor(R, dtype=torch.float)

        # Calculate the policy gradient
        log_probs = torch.stack(self.action_memory)
        policy_grad = -(log_probs * R).sum()
        
        # Backpropagate policy gradient
        policy_grad.backward()
        self.policy_optim.step()


def main(args):      
    episodes = 1000
    timesteps = 500
    log_interval = 25   # How often to print statistics

    log_interval_rewards = []
    total_rewards = []
    render_episode = True

    env = gym.make(args['env'])
    obs_space = env.observation_space.shape[0]
    act_space = env.action_space.n
    print("Observation space: {}\nAction space: {}".format(obs_space, act_space))

    agent = Trainer(obs_space, act_space)

    # Loop through episodes
    for episode in range(1, episodes + 1):
        obs = env.reset()

        # Loop until current episode is finished
        for t in range(1, timesteps + 1):
                    
            # Render every log_interval episode
            if render_episode:
                env.render()

            # Perform the action in the environment
            action = agent.choose_action(obs)
            new_obs, reward, done, _ = env.step(action)
    
            # Store the reward
            agent.store_rewards(reward)
            agent.store_state(obs)
            obs = new_obs

            if done:
                break

        agent.learn()  
        log_interval_rewards.append(sum(agent.reward_memory))       
        agent.reset_memory()    
        render_episode = False          

        if episode % log_interval == 0:
            print("Episode {} \t Rendered reward {:.3f}  \t Average reward {:.3f}".format(
                episode, log_interval_rewards[0], np.mean(log_interval_rewards)
            ))
            total_rewards.append(np.mean(log_interval_rewards))
            log_interval_rewards = []
            render_episode = True

    env.close()
    x_range = range(log_interval, (len(total_rewards) + 1) * log_interval, log_interval)
    plt.plot(x_range, total_rewards)
    plt.show()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('Perform deep Q-learning in an environment')
    parser.add_argument('--env', type=str, default='CartPole-v1',
                        help='An OpenAI environment to learn in. Default: CartPole-v0')
    args = parser.parse_args()
    main(vars(args))