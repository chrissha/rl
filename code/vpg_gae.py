"""
    Implements vanilla policy gradient together with the Generalized Advantage
    Estimator.
"""
import gym
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from torch.nn.functional import relu, softmax, smooth_l1_loss

# The policy network
class PolicyFunction(nn.Module):
    def __init__(self, act_space, obs_space, hidden_size=32):
        super().__init__()

        self.hidden = nn.Linear(obs_space, hidden_size)
        self.pi_layer = nn.Linear(hidden_size, act_space)
        self.v_layer = nn.Linear(hidden_size, 1)

    def forward(self, x):
        raise NotImplementedError

    def pi(self, x):
        x = relu(self.hidden(x))
        x = softmax(self.pi_layer(x), dim=1)
        return x

    def v(self, x):
        x = relu(self.hidden(x))
        x = self.v_layer(x)
        return x


class Trainer():
    def __init__(self, obs_space, act_space, lr=0.005, gamma=0.98, lmbda=0.95, baseline=False):
        self.learning_rate = lr
        self.gamma = gamma
        self.lmbda = lmbda

        # Check if we are using a baseline
        self.use_baseline = baseline

        # Create policy network
        self.policy = PolicyFunction(act_space, obs_space)
        self.policy_optim = torch.optim.Adam(self.policy.parameters(), lr=lr)

        self.data = []

    def store_transition(self, transition):
        self.data.append(transition)

    def get_batch(self):
        obs_lst, act_lst, rew_lst, newobs_lst, prob_lst, done_lst = [], [], [], [], [], []

        for transition in self.data:
            obs, act, rew, new_obs, prob, done = transition

            obs_lst.append(obs)
            act_lst.append([act])
            rew_lst.append([rew])
            newobs_lst.append(new_obs)
            prob_lst.append(prob)
            done_lst.append([0] if done is True else [1])   # Invert because multiply
        self.data = []

        return torch.tensor(obs_lst, dtype=torch.float), torch.tensor(act_lst), \
            torch.tensor(rew_lst, dtype=torch.float), torch.tensor(newobs_lst, dtype=torch.float), \
            torch.stack(prob_lst), torch.tensor(done_lst, dtype=torch.float)

    def choose_action(self, observation):
        # Select an action using the policy network
        observation = torch.tensor(observation, dtype=torch.float)
        probabilities = self.policy.pi(observation.reshape(1, -1)).squeeze()
        action_prob = torch.distributions.Categorical(probabilities)
        action = action_prob.sample().item()
        return action, probabilities

    def learn(self):
        # Calculates the gradient and updates parameters
        obs, actions, rewards, new_obs, probabilities, done = self.get_batch()

        td_target = rewards + self.gamma * self.policy.v(new_obs) * done
        delta = td_target - self.policy.v(obs)
        delta = delta.detach().numpy()

        """
        # Calculate discounted reward of GAE
        advantage_list = []
        advantage = 0
        for t, delta_t in enumerate(np.flip(delta)):
            advantage += (self.gamma * self.lmbda)**t * delta_t[0]
            advantage_list.append([advantage])
        advantage_list.reverse()

        advantages = torch.tensor(advantage_list).squeeze()
        #advantages -= advantages.mean()
        #advantages /= advantages.std() + 1e-7
        """

        # Compute the advantage function
        advantages = []
        advantage = 0.
        for delta_t in delta[::-1]:
            advantage = self.gamma * self.lmbda * advantage + delta_t[0]
            advantages.append([advantage])
        advantages.reverse()
        advantages = torch.tensor(advantages, dtype=torch.float).squeeze()

        # Calculate the policy gradient
        log_probs = torch.log(probabilities)
        loss = -(log_probs * advantages).sum() + smooth_l1_loss(self.policy.v(obs), td_target.detach())

        # Backpropagate policy gradient
        self.policy_optim.zero_grad()
        loss.backward()
        self.policy_optim.step()


def main(args):      
    episodes = 10000
    timesteps = 500
    log_interval = 25   # How often to print statistics

    score = 0
    log_interval_rewards = []
    total_rewards = []
    render_episode = True

    env = gym.make(args['env'])
    obs_space = env.observation_space.shape[0]
    act_space = env.action_space.n
    print("Observation space: {}\nAction space: {}".format(obs_space, act_space))

    agent = Trainer(obs_space, act_space, baseline=args['baseline'])

    # Loop through episodes
    for episode in range(1, episodes + 1):
        obs = env.reset()

        # Loop until current episode is finished
        for t in range(1, timesteps + 1):
                    
            # Render every log_interval episode
            if render_episode:
                env.render()

            # Perform the action in the environment
            action, probs = agent.choose_action(obs)
            new_obs, reward, done, _ = env.step(action)
    
            # Store the reward
            agent.store_transition((obs, action, reward, new_obs, probs[action], done))
            score += reward
            obs = new_obs

            if done:
                break

        agent.learn()  
        log_interval_rewards.append(score)       
        render_episode = False     
        score = 0     

        if episode % log_interval == 0:
            print("Episode {} \t Rendered reward {:.3f}  \t Average reward {:.3f}".format(
                episode, log_interval_rewards[0], np.mean(log_interval_rewards)
            ))
            total_rewards.append(np.mean(log_interval_rewards))
            log_interval_rewards = []
            render_episode = True

    env.close()
    x_range = range(log_interval, (len(total_rewards) + 1) * log_interval, log_interval)
    plt.plot(x_range, total_rewards)
    plt.show()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('Perform deep Q-learning in an environment')
    parser.add_argument('--env', type=str, default='CartPole-v1',
                        help='An OpenAI environment to learn in. Default: CartPole-v0')
    parser.add_argument('--baseline', action='store_true',
                        help='If the algorithm should use the state-value function as baseline')
    parser.set_defaults(baseline=False)
    args = parser.parse_args()
    main(vars(args))