"""
    Implements deep Q-learning with a convolutional neural network as its
    Q-table.
"""
import gym
import torch
from random import random, choices
from cv2 import resize 
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from itertools import count


#The policy network of the agent
class Model(nn.Module):
    def __init__(self, obs_units, act_units):
        super().__init__()

        # Convolution layers
        self.cv1 = nn.Conv2d(in_channels=4, out_channels=16, kernel_size=8, stride=4)
        self.cv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=4, stride=2)
        
        # Size of the flattened tensor
        self.in_features = 9*9*32

        # Fully connected layers
        self.fc1 = nn.Linear(in_features=self.in_features, out_features=256)
        self.fc2 = nn.Linear(in_features=256, out_features=act_units)

    def forward(self, x):
        # A full forward pass through convolution and fully connected layer
        x = nn.functional.relu(self.cv1(x))
        x = nn.functional.relu(self.cv2(x))
        x = torch.flatten(x, start_dim=1)
        x = nn.functional.relu(self.fc1(x))
        x = nn.functional.relu(self.fc2(x))
        return x

"""Frame buffer class to easier manage the use of k 
last frames aswell as the transformation of the frames.
"""
class FrameBuffer():
    def __init__(self, max_frames):
        self.max_frames = max_frames
        self.buffer = []

    def add_frame(self, frame):
        frame = np.mean(frame, axis=2)
        frame = resize(frame, (84, 110))
        frame = frame[13:97,:]  # Outputs 84x84 image
        self.buffer.append(frame)
        if len(self.buffer) > self.max_frames:
            self.buffer.pop(0)
    
    def get_stack(self):
        return np.array(self.buffer).reshape(-1, 84, 84)

    def clear(self):
        self.buffer = []


"""Replay Memory class to easier manage the use of a
replay memory which retrieves batches of memories.
"""
class ReplayMemory:
    def __init__(self, mem_size):
        self.mem_size = mem_size
        self.obs = []
        self.action = []
        self.reward = []
        self.new_obs = []
        self.done = []
    
    def add(self, memory):
        self.obs.append(memory[0])
        self.action.append(memory[1])
        self.reward.append(memory[2])
        self.new_obs.append(memory[3])
        self.done.append(memory[4])
        if len(self.obs) > self.mem_size:
            self.obs.pop(0)
            self.action.pop(0)
            self.reward.pop(0)
            self.new_obs.pop(0)
            self.done.pop(0)
    
    def get(self, batch_size):
        idx_range = batch_size if len(self.obs) >= batch_size else len(self.obs)
        idx = choices(range(0, idx_range), k=batch_size)
        return ([self.obs[i] for i in idx], [self.action[i] for i in idx], 
                [self.reward[i] for i in idx], [self.new_obs[i] for i in idx], 
                [self.done[i] for i in idx])


class Trainer():
    def __init__(self, obs_space, act_space, device=None, lr=1e-2, gamma=0.9, k=4):
        self.learning_rate = lr
        self.gamma = gamma

        self.action_space = act_space
        self.observation_space = obs_space

        obs_units = obs_space.n if type(obs_space) is gym.spaces.Discrete else obs_space.shape[0]
        act_units = act_space.n
        self.model = Model(obs_units, act_units)
        self.device = device
        self.model.to(device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)

        self.replay_memory = ReplayMemory(1e6)
        self.frame_buffer = FrameBuffer(k)

        self.reward_memory = []
        self.action_memory = []

    def store_rewards(self, reward):
        # Stores the reward
        self.reward_memory.append(reward)

    def reset_memory(self):
        self.action_memory = []
        self.reward_memory = []

    def discounted_rewards(self, rew):
        #Calculate discounted rewards for a sequence.
        discounted_rew = []
        for n in range(len(rew)):
            discounted_rew.append(self.gamma**n * rew[n])
        return discounted_rew

    def choose_action(self, observation, epsilon):
        # Select an action using the policy network using epsilon greedy
        if random() < epsilon:
            return self.action_space.sample()
        else:
            observation = torch.tensor(observation, dtype=torch.float, device=self.device)
            q_values = self.model(observation.reshape(1, -1, 84, 84))
            return torch.argmax(q_values).item()

    def learn(self, transitions):
        # Takes a batch of replays and learns from it
        self.optimizer.zero_grad()

        # Find the current Q-values
        obs = torch.tensor(transitions[0], dtype=torch.float, device=self.device)
        actions = torch.tensor(transitions[1])
        current_q = self.model(obs)[torch.arange(len(actions)),actions]
        
        # Find the max future Q-values
        new_obs = torch.tensor(transitions[3], dtype=torch.float, device=self.device)
        future_q = torch.max(self.model(new_obs), dim=1).values

        # Calculate y which is different in terminal states
        done = torch.tensor(transitions[4], device=self.device)
        reward = torch.tensor(transitions[2], device=self.device)
        y = torch.where(done, reward, reward + self.gamma * future_q)

        # Huber loss
        loss = torch.nn.functional.smooth_l1_loss(y, current_q)

        # Backpropagation
        loss.backward()
        self.optimizer.step()



def main(env_name):            
    #episodes = 500     # Unused because we want to go to max_frames
    max_timestep = 500
    max_frames = 1e7    # Total amount of frames to train
    log_interval = 1   # How often to print statistics
    epsilon = 1.0
    batch_size = 32
    k = 4               # How often to perform an action (in frames)
    
    render_episode = True

    log_interval_rewards = []
    total_rewards = []

    env = gym.make(env_name['env'])
    env.frameskip = k
    assert type(env.action_space) is gym.spaces.Discrete, \
        "Action space have to be discrete."
    assert type(env.observation_space) is gym.spaces.Box, \
        "Observation space have to be continuous."
    print("Performing deep Q-learning on OpenAI environment {}".format(env_name['env']))

    # Set up CUDA and agent
    cuda = torch.device('cuda') if torch.cuda.is_available() else None
    agent = Trainer(env.observation_space, env.action_space, cuda, k=k)
    current_frame = 0
    
    # Loop through episodes
    for episode in count():
        obs = env.reset()

        # Loop until current episode is finished
        for t in count():
                    
            current_frame += 1
            # Render every log_interval episode
            if render_episode:
                #env.render()
                pass   

            # Perform no action the first k frames
            if t <= k:
                agent.frame_buffer.add_frame(obs)
                obs, reward, done, _ = env.step(0)
                continue
        
            # Anneal epsilon from 1 to 0.1 over the first million frames
            epsilon = 1 - current_frame / 1.1e6 if epsilon > 0.1 else epsilon
            # Choose and perform action
            frame_stack = agent.frame_buffer.get_stack()
            action = agent.choose_action(frame_stack, epsilon) if t % k != 0 else 0
            new_obs, reward, done, _ = env.step(action)

            # Store data
            agent.store_rewards(reward)
            agent.frame_buffer.add_frame(new_obs)
            new_stack = agent.frame_buffer.get_stack()
            agent.replay_memory.add((frame_stack, action, reward, new_stack, done))

            # Learn
            transitions = agent.replay_memory.get(batch_size)
            agent.learn(transitions)

            obs = new_obs

            if done:
                log_interval_rewards.append(sum(agent.reward_memory))       
                agent.reset_memory()
                agent.frame_buffer.clear()  # Want to clear on the start of an episode
                break
        
        if episode % log_interval == 0:
            print("Episode {} \t Rendered reward {:.3f}  \t Average reward {:.3f}".format(
                episode, log_interval_rewards[0], np.mean(log_interval_rewards)
            ))
            total_rewards.append(np.mean(log_interval_rewards))
            log_interval_rewards = []

        if current_frame > max_frames:
            break

    # Plot reward progress
    x_range = range(log_interval, (len(total_rewards) + 1) * log_interval, log_interval)
    plt.plot(x_range, total_rewards)
    plt.show()

    # Test the trained agent (without randomness)
    for episode in range(1, 10):
        obs = env.reset()
        episode_reward = 0

        # Loop until current episode is finished
        for t in count():

            if t <= k:
                agent.frame_buffer.add_frame(obs)
                obs, reward, done, _ = env.step(0)
                continue
                    
            # Render every log_interval episode
            env.render()

            # Perform the action in the environment
            agent.frame_buffer.add_frame(obs)
            frame_stack = agent.frame_buffer.get_stack()
            action = agent.choose_action(frame_stack, 0)
            new_obs, reward, done, _ = env.step(action)
    
            # Store the reward
            episode_reward += reward
            obs = new_obs

            if done:
                agent.reset_memory()              
                break
    
        print("Episode {} \t Reward {:.3f}".format(episode, episode_reward))

    env.close()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('Perform deep Q-learning in an environment')
    parser.add_argument('--env', type=str, default='Pong-v0',
                        help='An OpenAI environment to learn in. Default: CartPole-v0')
    args = parser.parse_args()
    main(vars(args))